package com.marathon.demo.controller;

import com.marathon.demo.services.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DemoController {

    @Autowired
    DemoService demoService;



    @GetMapping(value = "/find")
    public String addExchangeRate(
            Model model,
            @RequestParam(value = "ruc", required = false) String ruc) {
        model.addAttribute("company", demoService.getCompany(ruc));

        return "show";
    }
}
