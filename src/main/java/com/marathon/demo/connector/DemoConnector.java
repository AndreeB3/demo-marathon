package com.marathon.demo.connector;

import com.marathon.demo.bean.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class DemoConnector {


    private static final String token = "cXdlcnR5bGFtYXJja19zYUBob3RtYWlsLmNvBXF";
    private RestTemplate restTemplate;

    @Autowired
    public DemoConnector() {
        this.restTemplate = new RestTemplate();
    }

    public Company getCompany(String ruc) {

        String url = "http://wsruc.com/Ruc2WS_JSON.php?tipo=2&ruc=20600892470&token=cXdlcnR5bGFtYXJja19zYUBob3RtYWlsLmNvBXF";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("tipo", 2)
                .queryParam("ruc", ruc)
                .queryParam("token", token);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<Company> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                Company.class);

        return response.getBody();
    }
}
