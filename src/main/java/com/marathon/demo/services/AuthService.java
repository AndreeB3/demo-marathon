package com.marathon.demo.services;

public interface AuthService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
