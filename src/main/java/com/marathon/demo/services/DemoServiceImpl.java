package com.marathon.demo.services;

import com.marathon.demo.bean.Company;
import com.marathon.demo.connector.DemoConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoServiceImpl implements DemoService {

    private DemoConnector demoConnector;

    @Autowired
    public DemoServiceImpl(DemoConnector demoConnector) {
        this.demoConnector = demoConnector;
    }

    @Override
    public Company getCompany(String ruc) {
        return demoConnector.getCompany(ruc);
    }
}
