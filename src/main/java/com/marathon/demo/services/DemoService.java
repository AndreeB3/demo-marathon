package com.marathon.demo.services;

import com.marathon.demo.bean.Company;

public interface DemoService {

    Company getCompany(String ruc);
}
