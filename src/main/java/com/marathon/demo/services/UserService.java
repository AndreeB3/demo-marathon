package com.marathon.demo.services;

import com.marathon.demo.bean.User;

public interface UserService {

    void save(User user);

    User findByUsername(String username);
}
